public class StarPyramid{
    public static void main(String[] args) {
        System.out.print(starPattern(7));
    }

	public static String starPattern(int rows) {
		 if (rows <= 0)
			 return "-1";
		 else {
			String s = "";
		 	for (int i = 1 ; i <= rows ; i++) {
				for (int j = rows; j >= i; j--) {
					s = s + " ";
			 	 }
			 	for (int t = 1 ; t <= i ; t++) {
				 	if (t < i)
					 	s = s + "*" + " ";
				 	else 
					 	s = s + "*";
			 	 }s = s +"\n";
		 	}
		 	return s;
		}
	
	}
}

import java.io.*;

public class CopyFile {
    public static void main(String[] args) {
        String source = "SourceFile.txt";
        String dest = "DestinationFile.txt";
        boolean status = copyFile(source, dest);
        System.out.println(status);
    }
    
    public static boolean copyFile(String source, String dest) {
    	try {

            FileInputStream fin = new FileInputStream(source);
            FileOutputStream fout = new FileOutputStream(dest); 
            BufferedReader br = new BufferedReader(new InputStreamReader(fin));
            BufferedWriter bw = new BufferedWriter(new PrintWriter(fout));
            String line = "" ;
            while((line = br.readLine()) != null) {
            	bw.write(line + "\n");
            }
            br.close();
            bw.flush();
            bw.close();
            fin.close();
            fout.close();
    	}catch(Exception e) {
    		return false;
    	}
    	return true;
    }
}

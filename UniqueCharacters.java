import java.util.*;
public class UniqueCharacters{
    public static void main(String[] args) {
        String str = "ENGINEERING";
        Set<Character> uniqAlphs = getUniqueLetters(str);
        System.out.println(uniqAlphs);
    }

    public static Set<Character> getUniqueLetters(String str) {
	    Set<Character> uniqueAlphs = new TreeSet<Character>();
	    if(str == null)
		    return uniqueAlphs;
	    str = str.trim();
	    if(str.length() == 0)
		    return uniqueAlphs;
	    for(int i = 0;i < str.length();i++){
		    char ch = str.charAt(i);
		    if(Character.isLetter(ch) || Character.isDigit(ch))
		        uniqueAlphs.add(ch);
	    }
	    return uniqueAlphs;


    }
}


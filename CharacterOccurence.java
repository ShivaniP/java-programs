import java.util.Scanner;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
public class CharacterOccurence {
 public static Map getCharOccurence(String str) {

      char[] arr = str.toCharArray();
     
      HashMap<Character,Integer> map = new HashMap<Character,Integer>();
     
      for (Character j : arr ) {
       if(map.containsKey(j)) {
         map.put(j, map.get(j)+1);
       }
       else {
         map.put(j, 1);
       }
      }
        
        
         return map;
   
 
     }
 
 
 public static void main(String[] args) {
  Scanner sc = new Scanner(System.in);
  System.out.println("Enter a string :");
  String str = sc.next();
  Map map = getCharOccurence(str);
  System.out.println(map);
 }
}

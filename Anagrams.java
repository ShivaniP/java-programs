import java.util.*;

public class Anagrams {

    public static void main(String[] args) {

        String[] words = {"listen", "silent", "elbow", "part", "panel", "trap", "tensil", "alter", "later", "below"};

        List<String> anagrams = findAnagrams(words);

        for (String word : anagrams) {

            System.out.println(word);

        }

    }


    public static List<String> findAnagrams(String[] words) {

	List<String> anagrams = new ArrayList<String>();
	
	Map<String , String> anagramsDict = new LinkedHashMap<String, String>();
	for (String word : words){
		word = word.toLowerCase();
		String key = generateKey(word);
		if (anagramsDict.containsKey(key)){
			anagramsDict.put(key, (String)anagramsDict.get(key)+","+ word);
		}else{
			anagramsDict.put(key,word);
		}
	}
	
	for (String key : anagramsDict.keySet()){
		String val = anagramsDict.get(key);
		if (val.indexOf(",") > 0){
			anagrams.add(val);
		}
	}
	return anagrams;
   
    }
 

    public static String generateKey(String word){
	char[] letters = word.toCharArray();
	Arrays.sort(letters);
	return new String(letters);
    }
}

public class StarPyramid01 {
	public static void main(String[] args) {
		System.out.print(starPattern(6));
	}
	public static String starPattern(int rows) {
		int i, j, k;
		String S = "";
		if (rows < 0) {
			S = S + "-1";
		} else if (rows == 0 ) {
			S = S + "-2";
		} else {
			for (i = 1; i <= rows; i++) {
				for (j = 1; j <= rows-i; j++) {
					S = S + " ";
				}
				for (k = 0; k != 2*i-1; k++) {
					S = S + "*";
				}
				S = S + "\n";
			}
		}
		return S;
	}
}

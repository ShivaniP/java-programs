import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
       Scanner scan = new Scanner(System.in);
       BigInteger num1 = scan.nextBigInteger();
       BigInteger num2 = scan.nextBigInteger();
       System.out.println(num1.add(num2));
       System.out.println(num1.multiply(num2));
    }
}

